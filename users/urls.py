from django.urls import path, include
from . import views
from . import cabinet_views as cabinet

urlpatterns = [
    path('cabinet/', cabinet.settings),
    path('my-products/', cabinet.products),
]

phones_urls = [
    path('cabinet/add-phone/', cabinet.add_phone),
    path('cabinet/delete-phone/<int:phone_id>', cabinet.delete_phone)
]

auth_urls = [
    path('login/', views.login_view),  # /users/login/
    path('register/', views.register_view),  # /users/register/
    path('logout/', views.logout_view),  # /users/logout/
]

urlpatterns += auth_urls
urlpatterns += phones_urls
