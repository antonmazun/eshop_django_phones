from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from .forms import RegisterForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.


def login_view(request):
    ctx = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        auth_user = authenticate(username=username, password=password)
        if auth_user:
            login(request, auth_user)
            return redirect('/')
        else:
            ctx['error'] = 'User not found!'
        print(username, password)
    return render(request, 'users/login.html', ctx)


def register_view(request):
    context = {
        'form': RegisterForm()
    }

    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = user.username
            password = form.cleaned_data['password1']
            print('django_password', user.password)
            print('valid form!', username, password)
            auth_user = authenticate(username=username, password=password)
            if auth_user:
                login(request, auth_user)
                return redirect('/')
        else:
            context['form'] = form
    return render(request, 'users/register.html', context)


def logout_view(request):
    logout(request)
    return redirect('/')
